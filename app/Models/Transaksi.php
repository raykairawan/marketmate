<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    use HasFactory;
    protected $table = 'transaksi'; 
    protected $fillable = ['id_user', 'id_produk', 'nama_customer', 'jumlah_produk', 'total_harga'];

    // Relasi dengan produk
    public function produk()
    {
        return $this->belongsTo(Produk::class, 'id_produk');
    }

    // Relasi dengan user
    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }
}

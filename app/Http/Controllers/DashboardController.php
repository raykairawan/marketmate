<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produk;
use App\Models\Transaksi;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $produks = Produk::all();
        $transaksis = Transaksi::all();
        return view('dashboard', ['name' => $user->name], compact('produks', 'transaksis'));
    }
}

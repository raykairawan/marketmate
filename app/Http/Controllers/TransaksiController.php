<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Produk;
use App\Models\Transaksi;
use Illuminate\Support\Facades\Auth;
use Dompdf\Dompdf;
use Dompdf\Options;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $name = Auth::user()->name;         
        $produks = Produk::all(); 
        $transaksis = Transaksi::query();
        
        if ($request->has('customer')) {
            $transaksis->where('nama_customer', 'like', '%' . $request->customer . '%');
        }

        $transaksis = $transaksis->paginate(10);

        return view('dashboard', compact('name', 'produks', 'transaksis'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $user = Auth::user();
        $produks = Produk::all();
        return view('transaksi.create', compact('user', 'produks'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_produk' => 'required|exists:produk,id',
            'nama_customer' => 'required|string|max:50',
            'jumlah_produk' => 'required|integer',
            'total_harga' => 'required|numeric',
        ]);

        $produk = Produk::findOrFail($request->id_produk);

        // Periksa apakah stok cukup
        if ($produk->jumlah_produk < $request->jumlah_produk) {
            return redirect()->back()->with('error', 'Stok produk tidak mencukupi.');
        }

        $produk->jumlah_produk -= $request->jumlah_produk;
        $produk->save();

        $id_user = Auth::id();

        $request->merge(['id_user' => $id_user]);


        // Buat transaksi baru
        Transaksi::create($request->all());
        return redirect()->route('dashboard')->with('success', 'Transaksi berhasil ditambahkan.');
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $transaksi = Transaksi::findOrFail($id);
        $produks = Produk::all();
        $users = User::all();
        return view('transaksi.edit', compact('transaksi', 'produks', 'users'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'id_produk' => 'required|exists:produk,id',
            'nama_customer' => 'required|string|max:50',
            'jumlah_produk' => 'required|integer',
            'total_harga' => 'required|numeric',
        ]);

        $transaksi = Transaksi::findOrFail($id);
        $id_user = Auth::id();
        $produk = Produk::findOrFail($request->id_produk);

        if ($produk->jumlah_produk < $request->jumlah_produk) {
            return redirect()->back()->with('error', 'Stok produk tidak mencukupi.');
        }
        
        $produk->jumlah_produk += $transaksi->jumlah_produk;
        $produk->jumlah_produk -= $request->jumlah_produk;
        $produk->save();
    
        $transaksi->id_user = $id_user;
        $transaksi->id_produk = $request->id_produk;
        $transaksi->nama_customer = $request->nama_customer;
        $transaksi->jumlah_produk = $request->jumlah_produk;
        $transaksi->total_harga = $request->total_harga;
        $transaksi->save();

        return redirect()->route('dashboard')->with('success', 'Transaksi berhasil diperbarui.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $transaksi = Transaksi::findOrFail($id);
        $transaksi->delete();
        return redirect()->route('dashboard')->with('success', 'Transaksi berhasil dihapus.');
    }


    public function printAllPDF()
    {
        $transaksis = Transaksi::all();

        $content = '<h1>Daftar Transaksi</h1>';
        $content .= '<table border="1">';
        $content .= '<tr><th>ID Transaksi</th><th>Nama Customer</th><th>Jumlah Produk</th><th>Total Harga</th></tr>';

        foreach ($transaksis as $transaksi) {
            $content .= '<tr>';
            $content .= '<td>' . $transaksi->id . '</td>';
            $content .= '<td>' . $transaksi->nama_customer . '</td>';
            $content .= '<td>' . $transaksi->jumlah_produk . '</td>';
            $content .= '<td>' . $transaksi->total_harga . '</td>';
            $content .= '</tr>';
        }

        $content .= '</table>';

        $options = new Options();
        $options->set('isHtml5ParserEnabled', true);
        $options->set('isRemoteEnabled', true);

        $dompdf = new Dompdf($options);

        $dompdf->loadHtml($content);

        $dompdf->setPaper('A4', 'portrait');

        $dompdf->render();

        return $dompdf->stream("all_transaksi.pdf");
    }
}

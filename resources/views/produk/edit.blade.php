<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Produk</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <form method="POST" action="{{ route('produk.update', $produk->id) }}">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="nama_produk">Nama Produk:</label>
                    <input type="text" class="form-control" id="nama_produk" name="nama_produk" value="{{ $produk->nama_produk }}" required>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="harga_produk">Harga Produk:</label>
                    <input type="number" class="form-control" id="harga_produk" name="harga_produk" value="{{ $produk->harga_produk }}" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="jumlah_produk">Jumlah Produk:</label>
                    <input type="number" class="form-control" id="jumlah_produk" name="jumlah_produk" value="{{ $produk->jumlah_produk }}" required>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</body>
</html>

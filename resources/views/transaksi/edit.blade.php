<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Transaksi</title>
    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <h2>Edit Transaksi</h2>
        <form method="POST" action="{{ route('transaksi.update', $transaksi->id) }}" class="needs-validation" novalidate>
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama_customer">Nama Customer:</label>
                <input type="text" class="form-control" id="nama_customer" name="nama_customer" value="{{ $transaksi->nama_customer }}" required>
                <div class="invalid-feedback">
                    Nama customer harus diisi.
                </div>
            </div>
            <div class="form-group">
                <label for="id_produk">Produk:</label>
                <select class="form-control" id="id_produk" name="id_produk" required>
                    @foreach($produks as $produk)
                        <option value="{{ $produk->id }}">{{ $produk->nama_produk }}</option>
                    @endforeach
                </select>
                <div class="invalid-feedback">
                    Pilih produk.
                </div>
            </div>
            <div class="form-group">
                <label for="jumlah_produk">Jumlah Produk:</label>
                <input type="number" class="form-control" id="jumlah_produk" name="jumlah_produk" required>
                <div class="invalid-feedback">
                    Jumlah produk harus diisi.
                </div>
            </div>
            <div class="form-group">
                <label for="total_harga">Total Harga:</label>
                <input type="number" class="form-control" id="total_harga" name="total_harga" required>
                <div class="invalid-feedback">
                    Total harga harus diisi.
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>
</body>
</html>
